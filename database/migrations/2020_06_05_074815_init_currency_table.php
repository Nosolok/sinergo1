<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class InitCurrencyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $data = [
            [
                'name' => 'Евро',
                'char_code' => 'EUR',
            ],
            [
                'name' => 'Доллар США',
                'char_code' => 'USD',
            ],
            [
                'name' => 'Украинская гривна',
                'char_code' => 'UAH',
            ],
            [
                'name' => 'Белорусский рубль',
                'char_code' => 'BYN',
            ],
        ];
        DB::Table('currency')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
