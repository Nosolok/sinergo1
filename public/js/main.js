const Main = { 
    template: `
        <div>
          <table class="ui celled table">
            <tr v-for="item in info" :key="item.id" >
              <td> {{ item.name }} </td>
              <td> {{ item.exchange_rate }} </td>
              <td>
                <i class="trash icon" :id="item.id" v-on:click="deleteCurrency"></i>
              </td>
            </tr>
          </table>

          <div class="ui message" v-if="addMessage">
            <p>{{ addMessage }}</p>
          </div>
        </div>
  `,

  data: function() {
    return {
      info: null,
      addStatus: null,
      addMessage: null
    }
  },

  mounted() {
    this.refreshCurrencies();
  },

  methods: {
    refreshCurrencies: function() {
      axios
      .get("/api/currency")
      .then(response => (this.info = response.data));
    },
    deleteCurrency: function (event){
      json = JSON.stringify({
        "curr_code": event.target.id
      });
      axios
        .delete("/api/currency/delete/" + event.target.id)
        .then(response => {
          this.addMessage = response.data.status;

          this.refreshCurrencies();

          setTimeout(() => {
            this.addMessage = null;
          }, 1500);
        } )
    }
  }
}



const AddCurrency = {
  template: `
    <div>
      <div class="ui input">
        <input type="text" placeholder="Название валюты" v-model="currName">
      </div>
      <div class="ui input">
        <input type="text" placeholder="Код валюты" v-model="currCode">
      </div>
      <button class="ui button" v-on:click="sendCurrency">
        Add
      </button>

      <div class="ui message" v-if="addMessage">
        <p>{{ addMessage }}</p>
      </div>

    </div>

  `,
  data: function() {
    return {
      currName: null,
      currCode: null,
      addStatus: null,
      addMessage: null
    }
  },

  methods: {
    sendCurrency: function(event) {
      json = JSON.stringify({
        "curr_name": this.currName,
        "curr_code": this.currCode
      });
      axios
        .post("/api/add", json)
        .then(response => {
          this.addMessage = response.data.status;

          setTimeout(() => {
            this.addMessage = null;
          }, 1500);
        } )
    }
  }
}


const routes = [
  { path: '/main', component: Main, props: true },
  { path: '/add', component: AddCurrency }
]

const router = new VueRouter({
  routes,
  linkActiveClass: "active item"
})



const app = new Vue({
  router,
  data() {
    return {
      info: null
    }
  },
}).$mount('#app')
