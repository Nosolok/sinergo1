<?php

namespace App\Http\Controllers;

use App\Models\Currency;
use Illuminate\Http\Request;
use App\Services\CurrencyService;

class CurrencyController extends Controller
{
    private $currencyService;

    public function __construct(CurrencyService $currencyService) {
        $this->currencyService = $currencyService;
    }

    public function index() {
        $currencyCodes = Currency::getAllCodes();
        
        if (sizeof($currencyCodes) > 0) {
            // update currency exchange rates if exist outdated records
            $exchangeRates = $this->currencyService->getCurrenciesFromCb($currencyCodes);
            Currency::updateExchangeRates($exchangeRates);            
        }

        $curr = Currency::all();
        
        return response()->json($curr);
    }
    
    public function addCurrency(Request $request) {
        $currencyName = $request->json('curr_name');
        $currencyCode = $request->json('curr_code');
        
        $result = Currency::firstOrCreate(
            ['name' => $currencyName],
            ['char_code' => $currencyCode]
        );
        if ($result->wasRecentlyCreated) {
            return response()->json(['status' => 'ok']);
        } else {
            return response()->json(['status' => 'already exist']);
        }
    }
    
    public function deleteCurrency($id) {
        $currency = Currency::where('id', $id)->first();

        if ($currency) {
            $result = $currency->delete();
            return response()->json(['status' => 'delete successfull']);
        } else {
            return response()->json(['status' => 'not found']);
        }
    }
    
}
