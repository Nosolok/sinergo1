<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class Currency extends Model {

    const CREATED_AT = null;
    const UPDATED_AT = null;

    protected $table = 'currency';
    
    protected $fillable = [
        'name',
        'char_code',
        'exchange_rate',
    ];

    /**
     * Get al currencies codes.
     *
     * @return array
     */
    public static function getAllCodes() {
        $date = Carbon::now()->subDays(1)->toDateTimeString();
        $codes = Currency::all('*')->where('updated_at', '<', $date)->pluck('char_code')->toArray();

        return $codes;
    }

    /**
     * Update currency exchange rates in DB.
     *
     * @param  array $currenciesList
     * @return void
     */
    public static function updateExchangeRates($currenciesList) {
        foreach ($currenciesList as $currency) {
            $currencyCode = array_keys($currency)[0];
            $currencyExchangeRate = $currency[$currencyCode];
            
            DB::table('currency')
                ->where('char_code', $currencyCode)
                ->update([
                    'exchange_rate' => $currencyExchangeRate,
                    'updated_at' => Carbon::now()
                ]);
        }
    }

}
