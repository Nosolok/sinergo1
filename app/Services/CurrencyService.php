<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;

class CurrencyService {
    
    public function __construct() {
    
    }

    /**
     * Request info about currencies from third-pary service.
     *
     * @param  array $currencyCodesList
     * @return array
     */    
    public function getCurrenciesFromCb($currencyCodesList) {
        $url = 'https://www.cbr-xml-daily.ru/daily_json.js';
        $response = Http::get($url);

        $exchangeRates = array();
        foreach ($currencyCodesList as $item) {
            foreach($response->json()['Valute'] as $exchangeRate) {
                if ($item ==  $exchangeRate['CharCode']) {
                    array_push($exchangeRates, [$exchangeRate['CharCode'] => $exchangeRate['Value']]);
                }
            }
        }

        return $exchangeRates;
    }
    
}