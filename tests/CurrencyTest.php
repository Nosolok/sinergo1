<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class CurrencyTest extends TestCase
{
    /**
     * Test adding new currency.
     *
     * @return void
     */
    public function testAddCurrency()
    {
        $testCurrencyName = 'testCurrency_' . rand();
        $testCurrencyCode = 'T' . rand(10, 99);
        
        $this->json('POST', '/api/add', [
            'curr_name' => $testCurrencyName,
            'curr_code' => $testCurrencyCode,
        ]);
        
        $this->seeInDatabase('currency', [
            'name' => $testCurrencyName,
            'char_code' => $testCurrencyCode
        ]);
        
    }
}
