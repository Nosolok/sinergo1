Склонировать проект:
```console
git clone https://Nosolok@bitbucket.org/Nosolok/sinergo1.git
```

Установить необходимые пакеты
```console
apt install unzip
apt install -y php7.4 --no-install-recommends
apt install -y php-mbstring php-xml php-zip php-db php-mysql 
apt install -y mysql-server --no-install-recommends
apt install -y apache2 --no-install-recommends
```

Скачать composer.phar:
```console
wget https://getcomposer.org/composer.phar
```

Запустить установку зависимостей:
```console
php composer.phar install
```

Создать конфиг-файл из примера:
```console
cp .env.example .env
```

В СУБД создать необходимую БД:
```console
create database sinergo1;
use sinergo1;
create user 'sinergo'@'localhost' identified by <пароль>;
grant all on sinergo1.* to 'sinergo'@'localhost';
```


и в конфиг-файле .env прописать данные для подключения к ней:
```ini
DB_DATABASE=sinergo1
DB_USERNAME=sinergo
DB_PASSWORD=<пароль>
```

Запустить миграцию БД:
```console
php artisan migrate
```

Включить модуль mod_rewrite
```console
a2enmod rewrite
systemctl restart apache2
```