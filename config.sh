#!/bin/bash

apt install unzip
apt install -y php7.4 --no-install-recommends
apt install -y php-mbstring php-xml php-zip php-db php-mysql 
apt install -y mysql-server --no-install-recommends

echo 'Загрузка composer'
wget https://getcomposer.org/composer.phar
echo 'Установка зависимостей'
php composer.phar install


cp .env.example .env
echo 'Генерация ключа приложения'
php artisan key:generate

echo 'Запуск миграции для БД'
php artisan migrate

