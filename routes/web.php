<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('currency', ['uses' => 'CurrencyController@index']);
    
    $router->post('add', ['uses' => 'CurrencyController@addCurrency']);
    
    $router->group(['prefix' => 'currency'], function ($id) use ($router) {
        $router->delete('delete/{id}', ['uses' => 'CurrencyController@deleteCurrency']);
    });
});